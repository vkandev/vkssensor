#ifndef _VKS_RETURN_CODES_H_
#define _VKS_RETURN_CODES_H_

enum VKS_RV
{
	VKS_RV_OK = 0,				            // OK, for all stages
	VKS_RV_MALLOCERROR,			            // Memory allocation error
	VKS_RV_IOERROR,				            // IO error
	VKS_RV_ARGUERROR,			            // Arguments error
	VKS_RV_DATAERROR,			            // Data error
	VKS_RV_UNKNOWN,				            // Error with unknown reason	
	
	// for enrollment
	VKS_ENROLL_FINGER_ENROLLED = 100,		// Success to generate template for current finger
	VKS_ENROLL_FINGER_REJECTED,	        	// Failed to generate template for current finger after maximum number trials of pressing
	VKS_ENROLL_SAMPLE_ACCEPTED,		        // The last pressing is good and accepted

	// for enrollment 
	VKS_ENROLL_PROMPT_MOVE_FINGER_MORE,			// Move finger more
	VKS_ENROLL_PROMPT_MOVE_FINGER_LESS,			// Do not move finger too much
	VKS_ENROLL_PROMPT_DUPLITE_FINGER,			// The last sample fail to be enrolled, prompt the user to move his/her finger if same positions in several trials
	
	// for verification
	VKS_VERIFY_SUCCESS,			            // Verification successful
	VKS_VERIFY_UPDATETPL_SUCCESS,	        // Verification successful, template adapted
	VKS_VERIFY_FAIL,				        // Verification fail, different from all registered 

	// Others
	VKS_RV_ABORT = 200,		                // Force quit
	VKS_RV_NO_SENSOR,				        // Cannot find valid VKS sensor
	VKS_RV_ABNORMAL_SENSOR,		            // Abnormal sensor status
	VKS_RV_ABNORMAL_ENGINE,		            // Abnormal engine status
	VKS_RV_ABNORMAL_CRYPTO,		            // Abnormal crypto status
	VKS_RV_ABNORMAL_PRESS,		            // Abnormal press status
	VKS_RV_NOFNGDATA,				        // Crypto IC no register finger data
	VKS_RV_NOFNGPRESS,			            // No finger press

	//Image quality
	VKS_RV_NULL_IMAGE = 300,		
	VKS_RV_POOR_IMAGE,			
	VKS_RV_GOOD_IMAGE,
	
	//HASP error
	VKS_RV_HASP_STATUS_OK = 450,
	VKS_RV_HASP_NO_TIME ,
	VKS_RV_HASP_FEATURE_EXPIRED,
	VKS_RV_HASP_DETACHED_LICENSE_FOUND,
	VKS_RV_HASP_LOGIN_ERROR,

	VKS_RV_TOTAL = 500			// Indicates total types of return value
};

#endif //_VKS_RESULT_H_

/**
  * vksReturnCodes.h
  * Author:		Evan Chen,		chenwenguang@vkansee.com
  *			    Judy Wang,		wangyuanjing@vkansee.com
  *			   	Layne Wang,		wanglei@vkansee.com
  *			    Lucas Wang,		lucaswang@vkansee.com
  *
  * Copyright(C) 2016 by Vkansee Technology Inc. All Rights Reserved.
  */
